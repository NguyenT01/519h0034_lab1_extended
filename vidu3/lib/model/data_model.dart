class DataModel {
  late int id;
  late String imgUrl;
  late String audioUrl;

  DataModel(this.id, this.imgUrl, this.audioUrl);

  String toString() {
    return '($id, $imgUrl, $audioUrl)';
  }
}