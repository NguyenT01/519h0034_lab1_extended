import 'package:flutter/material.dart';
import '../model/data_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:audioplayers/audioplayers.dart';


class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> {
  int counter = 0;
  List<DataModel> datas = [];
  var urlComponent = "https://thachln.github.io/toeic-data/ets/2016/1/p1/";
  AudioPlayer audioPlayer = AudioPlayer();


  fetchImages() async {
    var url = Uri.parse(
        'https://thachln.github.io/toeic-data/ets/2016/1/p1/data.json');
    var response = await http.get(url);
    var jsonData = response.body;

    print(jsonData);

    var jsonObject = jsonDecode(jsonData);

    for (int i = 0; i < jsonObject.length; i++) {
      datas.add(new DataModel(
          jsonObject[i]['no'],
          urlComponent + jsonObject[i]['image'],
          urlComponent + jsonObject[i]['audio']));
    }

    setState(() {
      // Do nothing
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchImages();
    super.initState();
  }

  play(url) async {
    int result = await audioPlayer.play(url);
    if (result == 1) {
      // success
    }
  }
  stop() async{
    await audioPlayer.stop();
  }

  @override
  Widget build(BuildContext context) {
    var appWidget = MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text('TOEIC App'),
          ),
          body: ListView.separated(
            scrollDirection: Axis.vertical,
            itemCount: datas.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                  padding: const EdgeInsets.only(left:8,right: 8),
                  margin: const EdgeInsets.only(top: 16),
                  height: 250,

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,

                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        //color: Colors.green,
                          child:

                            Image.network(
                              datas[index].imgUrl,
                              width: 250,
                              height: 300,
                            )
                        ),
                      const SizedBox(
                        width: 16,
                      ),
                      Column(
                        children: [
                          Text(
                            'Test ${datas[index].id}',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          const SizedBox(height: 48,),
                          FlatButton(
                            color: Colors.green,
                            textColor: Colors.white,
                            child: Text('PLAY',style: TextStyle(fontWeight: FontWeight.bold),),
                            onPressed: (){
                              play(datas[index].audioUrl);
                            },
                          ),
                          const SizedBox(height: 48,),
                          FlatButton(
                            color: Colors.red,
                            textColor: Colors.white,
                            child: Text('STOP',style: TextStyle(fontWeight: FontWeight.bold),),
                            onPressed: (){
                              stop();
                            },
                          )
                        ],
                      ),
                    ],
                  ));
            },
            separatorBuilder: (context, index) {
              return SizedBox(
                height: 16,
              );
            },
          ),
          // body: Container(
          //   child: Text(datas.length.toString()),
          // )
        ));

    return appWidget;
  }
}
